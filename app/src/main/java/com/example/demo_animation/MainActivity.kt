package com.example.demo_animation

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.animation.core.*
import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.border
import androidx.compose.foundation.layout.*
import androidx.compose.material.Button
import androidx.compose.material.ButtonDefaults
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.painter.Painter
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.Dp
import androidx.compose.ui.unit.dp

class MainActivity : ComponentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            val animationState = remember {
                mutableStateOf(false)
            }
            BoxWithConstraints(
                modifier = Modifier
                    .fillMaxSize()
                    .background(color = Color.Black)
            ) {
                Rocket(
                    isRocketEnabled = animationState.value,
                    maxWidth = maxWidth,
                    maxHeight = maxHeight)
                LaunchButton(animationState = animationState.value,
                    onToggleAnimationState = {
                        animationState.value = !animationState.value
                    })
            }
        }
    }
}

@Composable
fun Rocket(
    isRocketEnabled: Boolean,
    maxWidth: Dp,
    maxHeight: Dp,
) {
    val resource: Painter
    val rockSize = 200.dp
    val modifier: Modifier
    if (!isRocketEnabled) {
        resource = painterResource(id = R.drawable.rocket_intial)
        modifier = Modifier.offset(
            y = maxHeight - rockSize
        )
    } else {
        val infiniteTransition = rememberInfiniteTransition()
        val engineState = infiniteTransition.animateFloat(
            initialValue = 0f,
            targetValue = 1f,
            animationSpec = infiniteRepeatable(
                animation = tween(
                    durationMillis = 500,
                    easing = LinearEasing
                )
            ))

        val positionState = infiniteTransition.animateFloat(
            initialValue = 0f,
            targetValue = 1f,
            animationSpec = infiniteRepeatable(
                tween(
                    durationMillis = 2000,
                    easing = LinearEasing
                )
            ))

        resource = if (engineState.value <= 0.5f) {
            painterResource(id = R.drawable.rocket1)
        } else {
            painterResource(id = R.drawable.rocket2)
        }
        modifier = Modifier.offset(
            x = maxWidth * positionState.value,
            y = (maxHeight - rockSize) - ((maxHeight - rockSize) * positionState.value)
        )
    }

    Image(
        painter = resource,
        contentDescription = null,
        modifier = modifier
            .width(maxWidth - rockSize)
            .height(maxHeight - rockSize))
}

@Composable
fun LaunchButton(
    animationState: Boolean,
    onToggleAnimationState: () -> Unit,
) {
    Row(
        modifier = Modifier
            .fillMaxWidth()
            .padding(16.dp),
        horizontalArrangement = Arrangement.Center
    ) {

        if (animationState) {
            Button(
                onClick = onToggleAnimationState,
                colors = ButtonDefaults.buttonColors(
                    backgroundColor = Color.Red,
                    contentColor = Color.White
                )
            ) {
                Text(text = "Stop")
            }
        } else {
            Button(
                onClick = onToggleAnimationState,
            ) {
                Text(text = "Launch")
            }
        }
    }
}